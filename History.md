This document is deprecated. We now have a custom history server deployment project [here](https://gitlab.com/calincs/infrastructure/spark-history-server).

# Spark History Server

It would have been great if the Spark History Server Helm chart [here](https://github.com/helm/charts/tree/master/stable/spark-history-server) worked out of the box for us. Unfortunately, the Spark [docker image](https://github.com/lightbend/spark-history-server-docker) that comes with the chart has an incompatibility with access to Minio S3 storage. Except for one custom parameter, the Helm chart itself was sufficient but we went the way of composing a custom k8s deployment anyway.

It is important to note that the S3 server MUST have wildcard DNS (e.g. *.aux.lincsproject.ca) configured for the s3a access to work correctly.

## AWS Secrets

We first need to create a secret that will store the `aws_access_key` and `aws_secret_key` for S3 authentication so that the history server can read from the S3 buckets where the Spark jobs will write their logs.

```bash
kubectl create namespace spark-history
kubectl -n spark-history create secret generic aws-secrets --from-literal=aws-access-key=MY_KEY --from-literal=aws-secret-key=MY_SECRET
```

## Docker image

The docker image needs to contain Spark, Hadoop, and storage extensions (for S3 access). It is imperative that compatible versions of AWS and Hadoop libraries are used as Hadoop is compiled against specific versions. Here is a list of compatible versions:

* Hadoop 2.7: AWS S3 SDK 1.7.4
* Hadoop 2.8: AWS S3 SDK 1.10.6
* Hadoop 2.9.2 & 3.0: AWS S3 SDK 1.11.199
* Hadoop 3.1.0: AWS S3 SDK 1.11.271
* Hadoop 3.2.1: AWS S3 SDK 1.11.375

I could not get v1.7.4 of the AWS SDK to work with our Minio server. The current working [docker image](spark/Dockerfile) is using Hadoop 2.9.2 and AWS SDK 1.11.199

## Kubernetes deployment

Installing the helm chart with [this](spark/spark-history.yaml) config file or with these parameters resulted in an almost-working solution:

```bash
pvc.enablePVC=false
nfs.enableExampleNFS=false
s3.enableS3=true
s3.enableIAM=false
s3.secret=aws-secrets
s3.accessKeyName=aws-access-key
s3.secretKeyName=aws-secret-key
s3.logDirectory=s3a://spark-hs/log
s3.endpoint=https://aux.lincsproject.ca
service.type=ClusterIP
image.tag=v3.0.0
image.repository=zacanbot/spark-history-server
```

Installing the helm chart with the config file can be done like so:

```bash
helm repo add stable https://kubernetes-charts.storage.googleapis.com
helm install spark-history stable/spark-history-server --namespace spark-history -f spark-history.yaml
```

I could not get the s3a endpoints to work without adding `spark.hadoop.fs.s3a.path.style.access=true` to SPARK_HISTORY_OPTS, however. So, I just created a manifest file with all the templates from the Helm chart with this added env var set. Have a look at [spark-deployment.yaml](spark/spark-deployment.yaml) for the final all-in-one deployment.

## Alternatives

Another helm chart that I did not yet try is this one from banzaicloud: [https://banzaicloud.com/blog/spark-history-server/](https://banzaicloud.com/blog/spark-history-server/)

There might also be a docker image containing a newer version of Spark and Hadoop that will work as a history server that I did not try yet. Be mindful of the version dependencies between Hadoop and AWS though.
